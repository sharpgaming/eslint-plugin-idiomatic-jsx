import { RuleTester } from 'eslint';
import rule from '../prevent-attributes';
import parserOptionsMapper from './parserOptionsMapper';

const options = [{
  onClick: ['div', 'span']
}];

const attributeOptions = [{
  id: {
    components: ['div', 'span'],
    attributeNames: ['someAttribute']
  }
}];

const blackListOptions = [{
  onClick: {
    blacklist: true,
    components: ['a', 'button']
  }
}];

new RuleTester().run('prevent-attributes', rule, {
  valid: [
    {
      code: '<div></div>',
      options
    },
    {
      code: '<div><span style={{ color: \'red\' }}></span></div>',
      options
    },
    {
      code: '<a onClick={myFunction}></a>',
      options
    },
    {
      code: '<a onClick={myFunction}></a>',
      options: blackListOptions
    },
    {
      code: '<div someAttribute={true}></div>',
      options: attributeOptions
    }
  ].map(parserOptionsMapper),
  invalid: [
    {
      code: '<div onClick={null}></div>',
      options,
      errors: [
        '<div> must not have a "onClick" attribute.'
      ]
    },
    {
      code: '<div onClick={myFunction}></div>',
      options,
      errors: [
        '<div> must not have a "onClick" attribute.'
      ]
    },
    {
      code: '<span onClick={myFunction}></span>',
      options,
      errors: [
        '<span> must not have a "onClick" attribute.'
      ]
    },
    {
      code: '<div onClick={myFunction}></div>',
      options: blackListOptions,
      errors: [
        '<div> must not have a "onClick" attribute.'
      ]
    },
    {
      code: '<div onClick={myFunction}></div>',
      options: [...blackListOptions, (n, a) => `${n} ${a}`],
      errors: [
        'div onClick'
      ]
    },
    {
      code: '<div someAttribute={true} id="my-id"></div>',
      options: attributeOptions,
      errors: [
        '<div> must not have a "id" attribute.'
      ]
    }
  ].map(parserOptionsMapper)
});
