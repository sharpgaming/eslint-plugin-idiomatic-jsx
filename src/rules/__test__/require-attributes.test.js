import { RuleTester } from 'eslint';
import rule from '../require-attributes';
import parserOptionsMapper from './parserOptionsMapper';

const options = [{
  id: ['a', 'button']
}];

const attributeOptions = [{
  id: {
    components: ['div', 'span'],
    attributeNames: ['someAttribute']
  }
}];

const blackListOptions = [{
  id: {
    blacklist: true,
    components: ['div', 'span']
  }
}];

new RuleTester().run('require-attributes', rule, {
  valid: [
    {
      code: '<a id="my-id"></a>',
      options
    },
    {
      code: '<button id="my-id"></button>',
      options
    },
    {
      code: '<div></div>',
      options
    },
    {
      code: '<div></div>',
      options: blackListOptions
    },
    {
      code: '<div someAttribute={true} id="my-id"></div>',
      options: attributeOptions
    }
  ].map(parserOptionsMapper),
  invalid: [
    {
      code: '<a></a>',
      options,
      errors: [
        '<a> must have a valid "id" attribute.'
      ]
    },
    {
      code: '<button></button>',
      options,
      errors: [
        '<button> must have a valid "id" attribute.'
      ]
    },
    {
      code: '<a id={undefined}></a>',
      options,
      errors: [
        '<a> must have a valid "id" attribute.'
      ]
    },
    {
      code: '<a id=""></a>',
      options,
      errors: [
        '<a> must have a valid "id" attribute.'
      ]
    },
    {
      code: '<a id={null}></a>',
      options,
      errors: [
        '<a> must have a valid "id" attribute.'
      ]
    },
    {
      code: '<a id=""></a>',
      options: blackListOptions,
      errors: [
        '<a> must have a valid "id" attribute.'
      ]
    },
    {
      code: '<a id=""></a>',
      options: [...blackListOptions, (n, a) => `${n} ${a}`],
      errors: [
        'a id'
      ]
    },
    {
      code: '<div someAttribute={true}></div>',
      options: attributeOptions,
      errors: [
        '<div> must have a valid "id" attribute.'
      ]
    }
  ].map(parserOptionsMapper)
});
