import { hasAnyProp } from "jsx-ast-utils";

module.exports = (optionValue = [], componentType, componentAttributes = []) => {
  /* eslint no-nested-ternary: 0 */
  const components = Array.isArray(optionValue)
    ? optionValue
    : optionValue && typeof optionValue === "object"
    ? optionValue.components || []
    : [];

  const attributes = Array.isArray(optionValue)
    ? []
    : optionValue && typeof optionValue === "object"
    ? optionValue.attributeNames || []
    : [];

  const blacklist = (optionValue || {}).blacklist || false;

  // Only check specified elements
  const componentTypeCheck = !blacklist
    ? components.indexOf(componentType) >= 0
    : components.indexOf(componentType) < 0;

  const attributeNameCheck = !blacklist
    ? hasAnyProp(componentAttributes, attributes)
    : !hasAnyProp(componentAttributes, attributes);

  return !blacklist
    ? componentTypeCheck || attributeNameCheck
    : componentTypeCheck && attributeNameCheck;
};
